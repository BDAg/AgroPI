import requests
from lxml import html  #Até aqui, nós importamos as bibliotecas. (pode haver outras como BeatifulSoup etc. - depende do boot).

requisiçao = requests.get('https://www.agrolink.com.br/cotacoes/historico/rs/arroz-irrigado-em-casca-sc-50kg') #Aqui nos colocamos o site para requisição

tree = html.fromstring(requisiçao.content) #Aqui convertemos o html para o python conseguir trabalhar com ele

contador = 1 #Esse é o contador que vai variar as colunas da tabela
linha = 1 #Aqui é o valor inicial das linhas

while linha < 52: #Aqui nós falamos que enquanto a linha for menor que 11, ou seja, até 10, ele vai rodar o laço de repetição 
    mesAno = tree.xpath('//*[@id="content"]/div/div/div/div[1]/div[4]/div[1]/table/tbody/tr[%d]/td[1]/text()' %contador) #Aqui entra o xpath, não se esqueça de inserir o /text() ao final dele!(depende do codigo e da situaçao tbm)
    estadual = tree.xpath('//*[@id="content"]/div/div/div/div[1]/div[4]/div[1]/table/tbody/tr[%d]/td[2]/text()' %contador) #Aqui entra o xpath, não se esqueça de inserir o /text() ao final dele!(depende do codigo e da situaçao tbm)
    nacional = tree.xpath('//*[@id="content"]/div/div/div/div[1]/div[4]/div[1]/table/tbody/tr[%d]/td[3]/text()' %contador) #Aqui entra o xpath, não se esqueça de inserir o /text() ao final dele!(depende do codigo e da situaçao tbm)

    print (str (mesAno) +' | '+str(estadual)+' | '+str(nacional)) #aqui vamos printar as 3 variaveis - lembrando str= string (letras)

    linha = linha + 1 #aqui estamos somando 1 toda vez que o laço se repete
    contador = contador + 1 #aqui estamos somando 1 toda vez que o laço se repete

    print ('FIM DO BOOT ') #Aqui confirmamos a saida do Bot

'''
ALGUMAS DICAS PARA AJUDAR

Na linha 14,15 e 16 temos que inserir o xpath, inserir o /text() ao final dele e descobrir o que varia para colocarmos nosso contador no lugar!
O ideal é que ele fique como esse exemplo:
tree.xpath('/html/body/div[2]/main/div[%d]/div/div/text()' %contador) (ISTO É APENAS UM EXEMPLO!!!)
'''