from requests_html import HTMLSession
import json

# vc nao estava abrindo uma sessao
# pip install requests-html
# https://html.python-requests.org/

session = HTMLSession()
r = session.get('https://www.melhorcambio.com/euro-hoje')
data = r.html.xpath('//*[@id="comercial"]')
# o data retorna um vetor com todos os locais que queremos esse xpath
# por sorte o nosso valor eh a primeira casa do vetor.

# o data[0] tem um problema
# isso nao eh um texto eh um form preenchido

# usando o data[0].attrs teremos um json com os valores,
# entao importamos o json, carregamos o valor do data com o json.dumps
# o json ponto loads transforma o json.dumps em um variavel
# na qual podemos pesquisar os valores do json

print("\n na forma array \n")
print(data[0])
print("\n na forma json \n")
print(data[0].attrs)
valor = json.loads(json.dumps(data[0].attrs))

print("\n na forma json pesquisavel. \n")
print(valor["value"],"\n")
